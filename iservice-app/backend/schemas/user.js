const mongoose = require('mongoose');

module.exports = mongoose.model('Users', new mongoose.Schema({
    customerId: {
        type: String,
        required: true
    },

    country: {
        type: String,
        required: true
    },

    firstName: {
        type: String,
        required: true
    },

    lastName: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                var result = false;
                
                if (v.split('@').length == 2){
                    if (v.split('@')[1].split('.').length >= 2) {
                        result = true;
                    }
                } 

                return result;
            },
            message: props => `${props.value} is not a email`
        }
    },

    password: {
        type: String,
        required: true
    },

    address: {
        type: String,
        required: true
    },

    city: {
        type: String,
        required: true
    },

    state: {
        type: String,
        required: true
    },

    role: {
        type: String,
        required: true
    },

    zip: Number,

    mobile: Number,

    avatar: String
}));