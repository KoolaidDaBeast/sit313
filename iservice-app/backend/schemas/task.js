const mongoose = require('mongoose');

module.exports = mongoose.model('Tasks', new mongoose.Schema({

    id: {
        type: String,
        required: true
    },

    type: {
        type: Number,
        required: true
    },

    title: {
        type: String,
        required: true
    },

    desc: {
        type: String,
        required: true
    },

    image: {
        type: String,
        required: true
    },

    suburb: {
        type: String,
        required: true
    },

    date: {
        type: String,
        required: true
    },

    budgetType: {
        type: Number,
        required: true
    },

    budget: {
        type: Number,
        required: true
    },
}));