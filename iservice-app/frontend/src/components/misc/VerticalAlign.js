import React from 'react'
import { Grid } from 'semantic-ui-react'

export default function VerticalAlign({ html }) {
    return (
        <Grid verticalAlign='middle' className='fluid-height'>
            <Grid.Row verticalAlign='middle'>
                <Grid.Column>
                    {html}
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}
