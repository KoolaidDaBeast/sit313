import React from 'react'
import { Icon, Button, Grid, Input, Label, Ref } from 'semantic-ui-react'

export default function Footer() {
    const forwardedRef = React.createRef(null);

    return (
        <Grid style={{ paddingTop: '15px'}} verticalAlign='middle' className='footer'>
            <Grid.Row style={{ backgroundColor: 'lightgrey' }}>
                <Grid.Column width={7}>
                    <Label htmlFor="email"><b>NEWSLETTER</b></Label>
                    <Input placeholder='Enter Email...' style={{ paddingRight: '5px', paddingLeft: '5px' }} id="email" size='mini' />
                    <Ref innerRef={forwardedRef}>
                        <Button as='button' size='mini'>Subscribe</Button>
                    </Ref>
                </Grid.Column>

                <Grid.Column width={8} textAlign='right'>
                    <Label htmlFor="connect-us">CONNECT US</Label>
                    <Icon name='facebook' id="connect-us" style={{ paddingLeft: '5px' }} />
                    <Icon name='twitter' style={{ paddingLeft: '5px' }} />
                    <Icon name='instagram' style={{ paddingLeft: '5px' }} />
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}
