import React from 'react'
import { Container, Grid, Header, Icon } from 'semantic-ui-react'

export default function InvalidUrl() {
    return (
        <Container fluid>
            <Grid verticalAlign='middle' textAlign='center' style={{ height: '85vh' }}>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Icon name='ban' size='massive' />
                        <Header as='h1' style={{ fontSize: '5vw' }}>404
                            <Header.Subheader style={{ fontSize: '2vw', color: 'grey' }}>
                                Page not found
                            </Header.Subheader>
                            <Header.Subheader style={{ fontSize: '1vw', color: 'darkgrey' }}>
                                &nbsp;
                                The page you are looking for does not exist or an error has occured
                            </Header.Subheader>
                            <Header.Subheader style={{ fontSize: '1vw', color: 'darkgrey' }}>
                                &nbsp;
                                or you do not have permission to view this page.
                            </Header.Subheader>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}
