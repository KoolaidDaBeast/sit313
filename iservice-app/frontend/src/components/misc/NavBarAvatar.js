import React from 'react'
import { Image } from 'semantic-ui-react'

export default function NavBarAvatar({ source, name }) {
    return (
        <span>
            <Image src={source} avatar /> {name}
        </span>
    )
}
