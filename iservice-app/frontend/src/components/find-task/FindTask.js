import React, { useEffect, useState } from 'react'
import { Header, Card, Divider, Icon, Container } from 'semantic-ui-react'
import Task from './Task'
import TaskSearchBar from './TaskSearchBar'
import PlaceholderCard from '../misc/PlaceholderCard'
import { getApiEndpoint, fetchRequest } from '../../utility/RequestUtility'

async function getTasks() {
    return fetchRequest(`${getApiEndpoint()}/tasks`);
}

function filterTasks(tasks, searchValue, filter) {
    var updatedFilteredTasks = [];

    if (searchValue.length === 0) {
        updatedFilteredTasks = tasks;
    }
    else {
        tasks.forEach((task, index) => {
            if (task.title.toLowerCase().startsWith(searchValue) && filter === 'taskTitle') {
                updatedFilteredTasks.push(task);
            }
            else if (task.suburb.toLowerCase().startsWith(searchValue) && filter === 'taskSuburb') {
                updatedFilteredTasks.push(task);
            }
            else if (task.date.toLowerCase().startsWith(searchValue) && filter === 'taskDate') {
                updatedFilteredTasks.push(task);
            }
        });
    }

    return updatedFilteredTasks;
}

export default function FindTask() {
    const [loadingState, setLoadingState] = useState(true);
    const [tasks, setTasks] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const [filter, setFilter] = useState('taskTitle');
    const [filteredTasks, setFilteredTasks] = useState([]);

    //Fetch data from backend server
    useEffect(() => {
        async function fetchData() {
            const fetchedTasks = await getTasks();
            setTasks(fetchedTasks.response.data);
            setFilteredTasks(fetchedTasks.response.data);
            setLoadingState(false);
        }

        fetchData();
    }, []);

    //Filters tasks whenever data changes
    useEffect(() => {
        setFilteredTasks(filterTasks(tasks, searchValue, filter));
    }, [tasks, filter, searchValue]);

    //Filter data by search bar
    const handleSearchBarUpdate = (searchValue, filter) => {
        setSearchValue(searchValue);
        setFilter(filter);
    };

    //Handle task deletions
    const handleTaskDelete = (taskId) => {
        setTasks(tasks.filter(task => task.id !== taskId));
        setFilteredTasks(tasks);
    };

    return (
        <div style={{ width: '95vw', paddingLeft: '5vw' }}>
            <Container fluid className='main-content'>
                <Divider horizontal className='divider-header'>
                    <Header as='h4'>
                        <Icon name='search' />
                        Task List
                    </Header>
                </Divider>

                <TaskSearchBar onUpdate={handleSearchBarUpdate} />

                <PlaceholderCard columns={5} rows={4} display={loadingState} />

                <Card.Group itemsPerRow={5} stackable>
                    {filteredTasks.map((task, index) => (
                        <Task key={index}
                            id={task.id}
                            type={task.type}
                            src={task.image}
                            title={task.title}
                            desc={task.desc}
                            suburb={task.suburb}
                            date={task.date}
                            budget={task.budget}
                            budgetType={task.budgetType}
                            onDelete={handleTaskDelete}
                        />
                    ))}
                </Card.Group>
            </Container>
        </div>
    )
}
