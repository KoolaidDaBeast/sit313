import React, { useState } from 'react'
import { Accordion, Header, Icon, Image, Label, Modal } from 'semantic-ui-react'

export default function TaskExpanded({ id, src, type, title, desc, suburb, date, budget, budgetType, triggerModal }) {
    const [open, setOpen] = React.useState(false);
    const [activeIndex, setActiveIndex] = useState(0);

    const handleClick = (e, data) => {
        var newIndex = data.index === activeIndex ? -1 : data.index;
        setActiveIndex(newIndex);
    };

    return (
        <Modal
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            trigger={triggerModal}
        >
            <Modal.Header>
                {type === 0 ? <Label content='InPerson' color='blue' /> : <Label content='Online' color='green' />}
            </Modal.Header>
            <Modal.Content image scrolling>
                <Image size='medium' src={src} />
                <Modal.Description>
                    <Header>
                        {title} <Label tag color='teal' size='mini'><Icon name='dollar' />{budgetType === 1 ? `${budget} per hour` : budget}</Label>
                        <Header.Subheader>
                            Issued at: {date}
                        </Header.Subheader>
                    </Header>
                    <Accordion>
                        <Accordion.Title active={activeIndex === 0} index={0} onClick={handleClick}>
                            <Icon name='dropdown' /> Description
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === 0}>
                            <p>{desc}</p>
                        </Accordion.Content>

                        <Accordion.Title active={activeIndex === 1} index={1} onClick={handleClick}>
                            <Icon name='dropdown' /> Suburb
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === 1}>
                            <p>{suburb}</p>
                        </Accordion.Content>
                    </Accordion>
                </Modal.Description>
            </Modal.Content>
            {/* <Modal.Actions>
            </Modal.Actions> */}
        </Modal>
    )
}
