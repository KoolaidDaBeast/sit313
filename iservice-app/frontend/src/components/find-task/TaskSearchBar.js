import React, { useState } from 'react'
import { Form } from 'semantic-ui-react'

const options = [
    { key: 0, text: 'Task Title', value: 'taskTitle' },
    { key: 1, text: 'Suburb', value: 'taskSuburb' },
    { key: 2, text: 'Date', value: 'taskDate' },
];

export default function TaskSearchBar({ onUpdate }) {
    const [filter, setFilter] = useState(options[0].value);

    const handleChange = (e, data) => {
        setFilter(data.value);
        console.log(data.value);
    };

    return (
        <>
            <Form>
                <Form.Group>
                    <Form.Input fluid placeholder='Enter search Text' width={14} onChange={(e, data) => { onUpdate(data.value.toLowerCase(), filter) }}/>
                    <Form.Select width={2}
                        fluid
                        options={options}
                        placeholder='Select filter by...'
                        defaultValue={options[0].value}
                        onChange={handleChange}
                    />
                </Form.Group>
            </Form>
        </>
    )
}
