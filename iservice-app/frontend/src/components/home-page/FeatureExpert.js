import React, { useEffect, useState } from 'react'
import { Header, Card } from 'semantic-ui-react'
import Expert from './Expert'
import { fetchCachedUser } from '../../utility/RequestUtility'

//Libaries
const faker = require('faker');

function generateExpertData(count) {
    var experts = [];

    for (let index = 0; index < count; index++) {
        experts.push({
            src: faker.image.imageUrl(),
            name: faker.name.findName(),
            description: faker.lorem.paragraph(),
            rating: faker.datatype.number(5)
        });
    }

    return experts;
}

export default function FeatureExpert({ setAuthState }) {
    const [experts, setExperts] = useState([]);
    useEffect(() => {
        if (fetchCachedUser() != undefined) {
            setAuthState(true);
        }

        setExperts(generateExpertData(9));
    }, [setAuthState]);

    return (
        <div style={{width: '90vw', paddingLeft: '5vw'}}>
            <Header as='h2' textAlign='center'> Feature Experts</Header>
            <Card.Group itemsPerRow={3} stackable>
                {experts.map((expert ,index) => (<Expert key={index} src={expert.src} name={expert.name} desc={expert.description} rating={expert.rating}/>))}
            </Card.Group>
        </div>
    )
}
