import React from 'react'
import { Image } from 'semantic-ui-react'

export default function Header() {
    return (
        <Image src="https://picsum.photos/seed/picsum/1920" fluid style={headerImageCSS}/>
    )
}

const headerImageCSS = {
    height: '45vh', 
    paddingBottom: '10px'
}