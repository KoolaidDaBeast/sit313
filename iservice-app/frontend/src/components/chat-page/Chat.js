import React, { useEffect, useState } from 'react'
import { Container, Card, Dimmer, Loader } from 'semantic-ui-react'
import { fetchRequest, getApiEndpoint } from '../../utility/RequestUtility'
import ChatSendBar from './ChatSendBar'
import ChatWindow from './ChatWindow'
import useSocket from '../../hooks/useSocket'
import ChatHeader from './ChatHeader'

function isURL(str) {
    return str.startsWith('http') && str.includes('://');
}

export default function Chat({ title }) {
    const chatID = window.location.href.split('/').pop();
    const [messages, setMessages] = useState([]);
    const [loading, setLoadingState] = useState(true);
    const [userCache, setUserCache] = useState({});
    const socket = useSocket();

    //Grab messages
    useEffect(() => {
        async function grabMessages() {
            var currentMessages = (await fetchRequest(`${getApiEndpoint()}/messages/${chatID}`)).response;
            var currentUserCache = userCache;

            for (let index = 0; index < currentMessages.length; index++) {
                var currentMessage = currentMessages[index];
                const messageAuthorId = currentMessage.authorId;

                //Cache user
                if (currentUserCache[messageAuthorId] == undefined) {
                    const response = (await fetchRequest(`${getApiEndpoint()}/account/user/${messageAuthorId}`)).response;
                    currentUserCache[messageAuthorId] = response.data;

                    //Edit message with added details
                    const avatarURL = isURL(response.data.avatar) ? response.data.avatar : `${getApiEndpoint(true)}/profile/${response.data.avatar}`;
                    currentMessage.avatar = avatarURL;
                    currentMessage.authorName = response.data.name;
                    currentMessages[index] = currentMessage;
                    continue;
                }

                //Edit message with cached details
                const avatarURL = isURL(currentUserCache[messageAuthorId].avatar) ? currentUserCache[messageAuthorId].avatar : `${getApiEndpoint(true)}/profile/${currentUserCache[messageAuthorId].avatar}`;
                currentMessage.authorName = currentUserCache[messageAuthorId].name;
                currentMessage.avatar = avatarURL;
                currentMessages[index] = currentMessage;
            }

            setLoadingState(false);
            setMessages(msgs => [...currentMessages, ...msgs]);
            setUserCache(currentUserCache);
        }

        grabMessages();
        // eslint-disable-next-line
    }, [chatID]);

    useEffect(() => {
        socket.on(`chat/${chatID}`, function (data) {
            setMessages(messages => [...messages, data]);
            console.log('[RECIEVE]', data);
        });
    
        return () => {
            socket = undefined;
        }
    }, []);

    return (
        <>
            {loading ?
                <Dimmer active={loading} page>
                    <Loader>Loading Messages...</Loader>
                </Dimmer> : undefined
            }

            <Container>
                <Card fluid className='fluid-height' centered raised>
                    <ChatHeader id={chatID} />
                    <ChatWindow messages={messages} />
                    <ChatSendBar id={chatID} />
                </Card>
            </Container>
        </>
    )
}
