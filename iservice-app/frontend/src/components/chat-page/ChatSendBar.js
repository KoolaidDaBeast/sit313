import React, { useRef, useState } from 'react'
import { Button, Card, Form, Input } from 'semantic-ui-react'
import useSocket from '../../hooks/useSocket';
import { fetchCachedUser } from '../../utility/RequestUtility'

export default function ChatSendBar({ id, disabled }) {
    const [message, setMessage] = useState('');
    const socket = useSocket();
    const messageRef = useRef();

    const handleMessageSend = (e, data) => {
        if (message.trim().length === 0) return;

        socket.emit(`chat/${id}`, {
            taskId: id,
            authorId: fetchCachedUser().id,
            authorName: fetchCachedUser().name,
            avatar: fetchCachedUser().avatar,
            date: (new Date()).toLocaleString(),
            message
        });

        console.log('[SEND]', message);
        messageRef.current.value = '';
        setMessage('');
    };

    const handleMessageChange = (e) => {
        setMessage(e.target.value);
    };

    return (
        <Card.Content extra>
            <Form>
                <Input fluid action disabled={disabled}>
                    <input ref={messageRef} onChange={handleMessageChange} />
                    <Button color='teal' icon='send' onClick={handleMessageSend} disabled={disabled} />
                </Input>
            </Form>
        </Card.Content>
    )
}
