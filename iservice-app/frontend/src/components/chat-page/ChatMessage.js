import React from 'react'
import { Comment } from 'semantic-ui-react'

export default function ChatMessage({avatar, author, date, message}) {
    return (
        <Comment>
            <Comment.Avatar src={avatar} style={{width: '35px', height: '35px'}} />
            <Comment.Content>
                <Comment.Author as='span'>{author}</Comment.Author>
                <Comment.Metadata>
                    <div>{date}</div>
                </Comment.Metadata>
                <Comment.Text>{message}</Comment.Text>
            </Comment.Content>
        </Comment>
    )
}
