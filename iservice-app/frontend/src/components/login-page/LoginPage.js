import React, { useState } from 'react'
import { Container, Divider, Form, Grid, Image, Segment } from 'semantic-ui-react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import { getApiEndpoint, fetchRequest, setUserCache, sendRequest } from '../../utility/RequestUtility'
import LoginTypeGoogle from './LoginTypeGoogle'

async function validate(email, password) {
    return await fetchRequest(`${getApiEndpoint()}/account/login?email=${email}&password=${password}`);
}

export default function LoginPage() {
    const [loading, setLoadingState] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);

    const handleEmailChange = async (e, data) => {
        setEmail(data.value);
    };

    const handlePasswordChange = async (e, data) => {
        setPassword(data.value);
    };

    const handleClick = async (e, data) => {
        setLoadingState(true);
        var response = (await validate(email, password)).response;

        if (!response.success) {
            Swal.fire('Oh no!', 'Incorrect login details', 'error');
            setLoadingState(false);
            return;
        }
        const avatarURL = response.data.avatar.startsWith(`${response.data.id}.`) ? `${getApiEndpoint(true)}/profile/${response.data.avatar}` : response.data.avatar;
        response.data.avatar = avatarURL;

        setUserCache(response.data);
        setLoadingState(false);
        setRedirect(true);
    };

    const handleGoogleLogin = async (profileObj) => {
        setLoadingState(true);

        if (profileObj === undefined) {
            Swal.fire('Oh no!', 'Failed to login into google account', 'error');
            setLoadingState(false);
            return;
        }

        console.log('google', profileObj);

        const serverResponse = (await sendRequest(`${getApiEndpoint()}/account/google/edit`, 'PUT', {
            id: profileObj.googleId,
            name: profileObj.givenName,
            email: profileObj.email,
            avatar:  profileObj.imageUrl
        })).response;

        console.log('server', serverResponse);

        if (serverResponse.success) {
            const avatarURL = serverResponse.data.avatar.startsWith(`${serverResponse.data.id}.`) ? `${getApiEndpoint(true)}/profile/${serverResponse.data.avatar}` : serverResponse.data.avatar;
            setUserCache({
                id: serverResponse.data.id,
                name: serverResponse.data.name,
                avatar: avatarURL,
                role: serverResponse.data.role
            });

            setLoadingState(false);
            setRedirect(true);
        }
    };

    return (
        <Container>
            <Grid verticalAlign='middle' textAlign='center' style={{ marginTop: '5%' }}>
                <Grid.Row>
                    <Grid.Column textAlign='left'>
                        <Segment raised>
                            <Image src='/logo.png' size='small' centered />
                            <Form>
                                <Form.Field>
                                    <label>Email</label>
                                    <Form.Input placeholder='Email' disabled={loading} onChange={handleEmailChange} />
                                </Form.Field>
                                <Form.Field>
                                    <label>Password</label>
                                    <Form.Input placeholder='Password' type='password' disabled={loading} onChange={handlePasswordChange} />
                                </Form.Field>
                                <Form.Button fluid onClick={handleClick} loading={loading} disabled={loading}>Log In</Form.Button>
                                <Divider horizontal>OR</Divider>
                                <LoginTypeGoogle onSuccess={handleGoogleLogin} disabled={loading} />
                            </Form>
                        </Segment>

                        <div>
                            Don't have an account? <Link to='/register'>Create one here</Link>
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            {redirect ? <Redirect to='/' /> : ''}
        </Container>
    )
}
