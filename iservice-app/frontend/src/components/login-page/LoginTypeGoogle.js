import React from 'react'
import { GoogleLogin } from 'react-google-login'
import { Button, Icon } from 'semantic-ui-react';

export default function LoginTypeGoogle({ onSuccess, disabled }) {
    const responseGoogle = (response) => {
        //console.log(`[${response.error}]`, response.details);
        const { accessToken, profileObj } = response;

        if (accessToken === undefined) {
            onSuccess(undefined);
            return;
        }

        onSuccess(profileObj);
    }

    return (
        <>
            <GoogleLogin
                clientId="369485809591-gauk53h0p297p7vppi08jaicocuujijt.apps.googleusercontent.com"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                uxMode='popup'
                cookiePolicy={'single_host_origin'}
                render={renderProps => (<Button fluid onClick={renderProps.onClick} disabled={renderProps.disabled || disabled} color='google plus'><Icon name='google'/> Sign In with Google</Button>)}
            />
        </>
    )
}
