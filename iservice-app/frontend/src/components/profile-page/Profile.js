import React, { useEffect, useState } from 'react'
import { Container, Dimmer, Divider, Form, Header, Icon, Loader, Image } from 'semantic-ui-react'
import Swal from 'sweetalert2';
import { fetchCachedUser, fetchRequest, getApiEndpoint, sendRequest, setUserCache } from '../../utility/RequestUtility'
import VerticalAlign from '../misc/VerticalAlign'

const FILE_LIMIT = 1500000;

function fileToBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(undefined);
    });
}

export default function Profile() {
    const [accountData, setAccountData] = useState({});
    const [loading, setLoadingState] = useState(true);
    const [imageData, setImageData] = useState(undefined);

    const handleChange = async (e, data) => {
        if (e.target.files.length === 0) return;

        //Check file size
        if (e.target.files[0].size > FILE_LIMIT) {
            Swal.fire('Yikes!', 'Looks like that image is too large!', 'error');
            return;
        }

        const fileInBase64 = await fileToBase64(e.target.files[0]);

        //Had trouble reading file
        if (fileInBase64 == undefined) {
            Swal.fire('Yikes!', 'Looks like we had an issue trying to upload your image', 'error');
            return;
        }

        setImageData(fileInBase64);
    };

    //Grab and display profile data
    useEffect(() => {
        fetchRequest(`${getApiEndpoint()}/account/user/${fetchCachedUser().id}`).then(result => {
            const response = result.response.data;

            if (response === undefined) return;

            setAccountData({
                country: response.country,
                firstName: response.name.split(' ')[0],
                lastName: response.name.split(' ')[1]
            });
            setLoadingState(false);
        });
    }, []);

    //Update profile
    const handleSaveProfile = (e, data) => {
        if (accountData.firstName.includes(' ') || accountData.lastName.includes(' ')) {
            Swal.fire('Oops!', 'First or last names cannot have spaces.', 'error');
            return;
        }

        sendRequest(`${getApiEndpoint()}/account/edit`, 'PUT', {
            id: fetchCachedUser().id,
            firstName: accountData.firstName,
            lastName: accountData.lastName,
            avatar: imageData
        }).then(result => {
            const response = result.response;

            //Grab latest profile data and reload page
            if (response.success) {
                setLoadingState(true);

                fetchRequest(`${getApiEndpoint()}/account/user/${fetchCachedUser().id}`).then(result => {
                    const response = result.response.data;
                    console.log(response);

                    if (response === undefined) return;

                    const avatarURL = response.avatar.startsWith(`${response.id}.`) ? `${getApiEndpoint(true)}/profile/${response.avatar}` : response.avatar;
                    response.avatar = avatarURL;

                    setUserCache(response);
                    window.location.reload();
                });
            }
        });
    };

    const handleFirstNameChange = (e, data) => {
        setAccountData({
            country: accountData.country,
            firstName: data.value,
            lastName: accountData.lastName
        });
    };

    const handleLastNameChange = (e, data) => {
        setAccountData({
            country: accountData.country,
            firstName: accountData.firstName,
            lastName: data.value
        });
    };

    return (
        <>
            {loading ?
                <Dimmer active={loading} page>
                    <Loader>Loading...</Loader>
                </Dimmer> : undefined
            }

            <Container>
                <VerticalAlign html={
                    <>
                        <Divider horizontal className='divider-header'>
                            <Header as='h4'>
                                <Icon name='user' />
                                Update Details
                            </Header>
                        </Divider>

                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='First name' placeholder='First name' defaultValue={accountData.firstName} onChange={handleFirstNameChange} />
                                <Form.Input fluid label='Last name' placeholder='Last name' defaultValue={accountData.lastName} onChange={handleLastNameChange} />
                            </Form.Group>

                            <Divider horizontal className='divider-header'>
                                <Header as='h4'>
                                    <Icon name='users' />
                                    Update Avatar
                                </Header>
                            </Divider>

                            <Form.Group>
                                <Image src={imageData || fetchCachedUser().avatar} centered className='img-preview' />
                            </Form.Group>

                            <Form.Group>
                                <input className='fluid-input' type='file' accept="image/png, image/jpeg" onChange={handleChange} />
                            </Form.Group>

                            <Form.Group widths='equal'>
                                <Form.Button color='green' fluid onClick={handleSaveProfile}>Save Profile</Form.Button>
                            </Form.Group>
                        </Form>
                    </>
                } />
            </Container>
        </>
    )
}
