import React, { useContext, useState } from 'react'
import { Container, Form, Grid, Image, Label, Segment } from 'semantic-ui-react'
import Swal from 'sweetalert2'
import { PostTaskContext } from '../../contexts/PostTaskContext'

const FILE_LIMIT = 1500000;

function fileToBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(undefined);
    });
}

export default function TaskDescImage() {
    const { taskContextData, setTaskContextData } = useContext(PostTaskContext);
    const [imageData, setImageData] = useState(undefined);

    const handleChange = async (e, data) => {
        if (e.target.files.length === 0) return;

        //Check file size
        if (e.target.files[0].size > FILE_LIMIT){
            Swal.fire('Yikes!', 'Looks like that image is too large!', 'error');
            return;
        }

        const fileInBase64 = await fileToBase64(e.target.files[0]);

        //Had trouble reading file
        if (fileInBase64 == undefined) {
            Swal.fire('Yikes!', 'Looks like we had an issue trying to upload your image', 'error');
            return;
        }

        //Store image in context
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors, { taskImage: false });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskImage: fileInBase64,
            formErrors: updatedFormErrors
        });

        setImageData(fileInBase64);
        setTaskContextData(updatedTaskContextData);
    };

    return (
        <Container fluid>
            <Form>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Label>Upload your image: {FILE_LIMIT / 100}KB</Label>
                        </Grid.Column>
                        <Grid.Column width={13} textAlign='center'>
                            <Segment placeholder>
                                <Image src={imageData} centered className='img-preview' />
                                <input className='fluid-input' type='file' accept="image/png, image/jpeg" onChange={handleChange} />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Form>
        </Container>
    )
}
