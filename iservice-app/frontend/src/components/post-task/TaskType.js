import React, { useContext, useState } from 'react'
import { Container, Divider, Grid, Header, Icon, Label, Radio } from 'semantic-ui-react'
import { PostTaskContext } from '../../contexts/PostTaskContext';

export default function TaskType({ onChange, loading }) {
    const [taskType, setTaskType] = useState(0);
    const { taskContextData, setTaskContextData } = useContext(PostTaskContext);

    const handleChange = (e, data) => {
        const type = parseInt(data.value);
        var updatedFormErrors = (type === 1) ? Object.assign({}, taskContextData.formErrors, {taskSuburb: false}) : Object.assign({}, taskContextData.formErrors, {taskSuburb: true});
        var updatedTaskContextData = Object.assign({}, taskContextData, {taskType: type, formErrors: updatedFormErrors});
        setTaskType(type);
        setTaskContextData(updatedTaskContextData);
    };

    return (
        <Container fluid>
            <Divider horizontal className='divider-header'>
                <Header as='h4'>
                    <Icon name='columns' />
                    Task Type
                </Header>
            </Divider>

            <Grid columns={2}>
                <Grid.Row>
                    <Grid.Column width={3}>
                        <Label className='lbl'> Select task type: </Label>
                    </Grid.Column>
                    <Grid.Column width={13}>
                        <Radio
                            label='In person'
                            name='taskTypeInPerson'
                            value='0'
                            checked={taskType === 0}
                            onChange={handleChange}
                            disabled={loading}
                        />

                        <Radio style={{ marginLeft: '10px' }}
                            label='Online'
                            name='taskTypeOnline'
                            value='1'
                            checked={taskType === 1}
                            onChange={handleChange}
                            disabled={loading}
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}
