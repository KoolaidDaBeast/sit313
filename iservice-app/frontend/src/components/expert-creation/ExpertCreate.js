import React, { useState } from 'react'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'
import { fetchCachedUser, sendRequest, getApiEndpoint, fetchRequest, setUserCache } from '../../utility/RequestUtility'

export default function ExpertCreate({ onUpdate }) {
    const [loadingState, setLoadingState] = useState(false);

    const handleClick = async (e, data) => {
        setLoadingState(true);

        //Edit user's data
        await sendRequest(`${getApiEndpoint()}/account/edit`, "PUT", { id: fetchCachedUser().id, role: 'expert' });

        //Wait few seconds for changes to take effect
        setTimeout(async () => {
            var fetchUpdatedUserData = (await fetchRequest(`${getApiEndpoint()}/account/user/${fetchCachedUser().id}`)).response;
            const avatarURL = fetchUpdatedUserData.data.avatar.startsWith(`${fetchUpdatedUserData.data.id}.`) ? `${getApiEndpoint(true)}/profile/${fetchUpdatedUserData.data.avatar}` : fetchUpdatedUserData.data.avatar;
            fetchUpdatedUserData.data.avatar = avatarURL;

            setUserCache(fetchUpdatedUserData.data);
            setLoadingState(false);
            onUpdate(false);
            onUpdate(true);
        }, 3000);
    };

    return (
        <Segment placeholder loading={loadingState}>
            <Header icon>
                <Icon name='handshake' />
                Would you like to become an expert?
            </Header>
            <Button primary onClick={handleClick} disabled={loadingState}>Become Expert</Button>
        </Segment>
    )
}
