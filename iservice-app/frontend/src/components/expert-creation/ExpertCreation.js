import React from 'react'
import { Container, Grid } from 'semantic-ui-react'
import ExpertCreate from './ExpertCreate'
import ExpertDelete from './ExpertDelete'
import { fetchCachedUser } from '../../utility/RequestUtility'

export default function ExpertCreation({ onRoleChange }) {
    return (
        <Container>
            <Grid style={{ height: '90vh' }} verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column>
                        {fetchCachedUser().role === 'customer' ? <ExpertCreate onUpdate={onRoleChange}/> : <ExpertDelete onUpdate={onRoleChange}/>}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}
