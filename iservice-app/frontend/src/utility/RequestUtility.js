module.exports = {
    getApiEndpoint: function(root) {
        return root ? 'http://localhost:3000' : 'http://localhost:3000/api';
        //return root ? 'https://iservice-backend.herokuapp.com' : 'https://iservice-backend.herokuapp.com/api';
    },

    fetchRequest: async function(url) {
        try {
            const response = await fetch(url);

            return { success: true, response: await response.json() };
        }
        catch {
            return { success: false, response: 'Cannot make a connection with the server at the moment! \nPlease try again later :)' };
        }
    },

    sendRequest: async function(url, method, data) {
        try {
            const response = await fetch(url, {
                method: method,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data),
            });

            return { success: true, response: await response.json() };
        }
        catch {
            return { success: false, response: 'Cannot make a connection with the server at the moment! \nPlease try again later :)' };
        }
    },

    setUserCache: function (data) {
        sessionStorage.setItem('jkt', JSON.stringify(data));
    },

    fetchCachedUser: function () {
        try {
            return JSON.parse(sessionStorage.getItem('jkt'));
        }
        catch {
            return {};
        }
    },

    clearCachedUser: function (data) {
        sessionStorage.removeItem('jkt');
    },
}