const io = require("socket.io-client");
var socket = io('http://localhost:5000/');

const useSocket = () => {
    return socket;
}

export default useSocket