//Compoments
import Header from './components/home-page/Header'
import FeatureExpert from './components/home-page/FeatureExpert'
import Footer from './components/misc/Footer'
import Navbar from './components/misc/Navbar'
import PostTask from './components/post-task/PostTask'
import InvalidUrl from './components/misc/InvalidUrl'
import FindTask from './components/find-task/FindTask'
import LoginPage from './components/login-page/LoginPage'
import RegisterPage from './components/register-page/RegisterPage'
import ExpertCreation from './components/expert-creation/ExpertCreation'
import Donate from './components/donate-page/Donate'

//Libraries
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom"
import { Container } from 'semantic-ui-react'
import { useState } from 'react'
import { fetchCachedUser } from './utility/RequestUtility'
import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import Chat from './components/chat-page/Chat'
import Profile from './components/profile-page/Profile'

//Constants
const stripePromise = loadStripe('pk_test_51JZuQRI0HeNEJSyCp6XGDqzabaVgRJIKSYZwucHdpGKhOCAxrFLVYH4Lp9Eh1YGI2Gl7Uo9AqLLQ0cv0GeFwcmIa007jT8Vg9b');


function App() {
  const [authState, setAuthState] = useState(fetchCachedUser() != undefined);

  return (
    <Elements stripe={stripePromise}>
      <Router>
        <Container fluid>
          <Navbar authed={authState} setAuthState={setAuthState} />

          <Switch>
            {/* Default Path */}
            <Route exact path='/'>
              <Header />
              <FeatureExpert setAuthState={setAuthState} />
              <Footer />
            </Route>

            {/* Post Task Path */}
            <Route exact path='/task/post'>
              {authState ? <PostTask /> : <Redirect to='/login' />}
            </Route>

            {/* Find Task Path */}
            <Route exact path='/task/find'>
              {authState ? <FindTask /> : <Redirect to='/login' />}
            </Route>

            {/* Become Expert Path */}
            <Route exact path='/expert/creation'>
              {authState ? <ExpertCreation onRoleChange={setAuthState} /> : <Redirect to='/login' />}
            </Route>

            {/* Login Path */}
            <Route exact path='/login'>
              {authState ? <Redirect to='/' /> : <LoginPage />}
            </Route>

            {/* Donate Path */}
            <Route exact path='/donate'>
              {authState ? <Donate /> : <Redirect to='/' />}
            </Route>

            {/* Register Path */}
            <Route exact path='/register'>
              {authState ? <Redirect to='/' /> : <RegisterPage />}
            </Route>

            {/* Profile Path */}
            <Route exact path='/profile/edit'>
              {authState ? <Profile /> : <Redirect to='/' />}
            </Route>

            {/* Task Chat Window Path */}
            <Route path='/chat/'>
              {authState ? <Chat /> : <Redirect to='/login' />}
            </Route>

            {/* Path not defined */}
            <Route path="*">
              <InvalidUrl />
            </Route>
          </Switch>
        </Container>
      </Router>
    </Elements>
  );
}

export default App;
