//Import Libraires 
var app = require('express')();
const cors = require('cors');
const mongoose = require('mongoose');
const server = require('http').Server(app);
const middleware = require('socketio-wildcard')();
const io = require('socket.io')(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

//Variables
const port = process.env.PORT || 5000;
const manager = require('./manager');

//Enable cross-origin support and start server
app.use(cors());
io.use(middleware);

//Connect to database
mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true });

io.on("connection", function (socket) {
    socket.on("*", async function (content) {
        const topic = content.data[0];
        const data = content.data[1];

        if (topic.startsWith('chat/')) {
            var message = await manager.addMessage(
                data.taskId,
                data.authorId,
                data.date,
                data.message,
            );
            message.avatar = data.avatar;
            message.authorName = data.authorName;
            io.emit(topic, message);
        }
        else {
            io.emit(topic, message);
        }
    });
});

server.listen(port);