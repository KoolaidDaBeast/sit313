const Messages = require('./schemas/message');
const crypto = require('crypto');

module.exports = {
    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    addMessage: async function (taskId, authorId, date, message) {
        const id = this.hash(Date.now().toString(), Date.now().toString());
        
        const newMessage = new Messages({
            id: id,
            taskId: taskId, 
            authorId: authorId,
            message: message,
            date: date,
            sentAt: Date.now()
        });

        await newMessage.save(err => { });

        return { id, authorId, taskId, date, message };
    },

    updateTaskData: async function (taskId, taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget) {
        var result = false;

        var promise = Messages.findOne({ id: taskId }, async (err, document) => {
            if (document != undefined) {
                document.type = taskType || document.type;
                document.title = taskTitle || document.title;
                document.desc = taskDesc || document.desc;
                document.suburb = taskSuburb || document.suburb;
                document.date = taskDate || document.date;
                document.budgetType = budgetType || document.budgetType;
                document.budget = taskBudget || document.budget;
                result = true;

                await document.save(err => { });
            }
        });

        const promiseValue = await promise;
        console.log(promiseValue);
        return result ? { success: true } : { success: false };
    },

    deleteMessageId: async function (id) {
        var promise = Messages.deleteOne({ id: taskId });

        const promiseValue = await promise;

        return promiseValue || {};
    },
}