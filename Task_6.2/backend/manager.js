const Users = require('./schemas/user');
const Experts = require('./schemas/expert');
const crypto = require('crypto');
const mailer = require('@sendgrid/mail');
const { resolve } = require('path');

module.exports = {
    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    setEmailDetails: function (key) {
        mailer.setApiKey(key);
    },

    sendEmail: function (to, subject, text) {
        const msg = {
            to,
            from: 'electrotech14@hotmail.com', // Use the email address or domain you verified above
            subject,
            text
        };

        mailer.send(msg).then(() => { }, error => {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        });
    },

    registerAccount: async function (country, firstName, lastName, email, password, address, city, state, zip, mobile) {
        var errors = [];
        email = email.toLowerCase();

        const newUser = new Users({
            customerId: this.hash(email, firstName + lastName),
            country: country,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: this.hash(email, password),
            address: address,
            city: city,
            state: state,
            zip: zip,
            mobile: mobile
        });

        var error = newUser.validateSync();
        console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        var promise = newUser.save(err => { });
        await promise;

        if (errors.length == 0) {
            this.sendEmail(email,
                "IServiceDB Welcome",
                `Hi ${firstName},
                Welcome to the IServiceDB platform!
                We perform many things like 3d printing, pcb making and much more!

                Thanks,
                IServiceDB Team
                `);
        }

        return errors;
    },

    verifyLoginDetails: async function (email, password) {
        var promise = Users.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;
        const hashedPassword = this.hash(email, password);

        return promiseValue == undefined ? false : (hashedPassword == promiseValue.password);
    },

    customerIdExists: async function (key) {

        var promise = Accounts.findOne({ customerId: key });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getCustomerId: async function (email) {
        var promise = Users.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.customerId;
    },

    accountExists: async function (email) {

        var promise = Users.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    /////// EXPERTS ///////

    expertIdExists: async function (id) {

        var promise = Experts.findOne({ expertId: id });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getExpertData: async function (expertId) {
        var promise = Experts.findOne({ expertId: expertId });

        const promiseValue = await promise;

        return promiseValue || {};
    },

    addExpert: async function (email, password, address, mobile) {
        var errors = [];
        email = email.toLowerCase();
        const id = this.hash(Date.now().toString(), Date.now().toString());

        const newExpert = new Experts({
            expertId: id,
            email: email,
            password: this.hash(email, password),
            address: address,
            mobile: mobile
        });

        var error = newExpert.validateSync();
        console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        await newExpert.save(err => { });

        return { errors: errors, id: id };
    },

    updateExpertData: async function (expertId, newEmail, newPassword, newAddress, newMobile) {
        var result = false;

        var promise = Experts.findOne({ expertId: expertId }, (err, document) => {
            if (document != undefined) {
                const newHashedPassword = this.hash(newEmail || document.email, newPassword || document.password);
                document.email = newEmail || document.email;
                document.password = newHashedPassword || document.password;
                document.address = newAddress || document.address;
                document.mobile = newMobile || document.mobile;

                result = true;
    
                document.save(err => { });
            }
        });

        const promiseValue = await promise;

        return result == true ? {success: true} : {success: false};
    },

    deleteExpertData: async function (expertId) {
        var promise = Experts.deleteOne({ expertId: expertId });

        const promiseValue = await promise;

        return promiseValue || {};
    },
}