const express = require('express');
const mongoose = require('mongoose');
const manager = require('./manager');
const app = express();
const session = require('express-session');
const stripe = require('stripe')('sk_test_51JZuQRI0HeNEJSyCD3BTPWNC85sT8NdynAbeAaJgCKy0QrjoEI2P5qBpRACEE8RUxdaSGCAOFeprHChqL0NnyYGC00mBCJGLXP');

//Static Variables
const port = process.env.PORT || 3000;

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});

//Setting express to encode reques
app.use(express.urlencoded({ extended: true }));

//Connect to the database
mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true });
manager.setEmailDetails(process.env.SENDGRID_API_KEY);

app.set('view engine', 'ejs');

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'SECRET'
}));

app.get('/', (req, res) => {
    return res.json({success: true, message: "REST api server is up!"});
});

app.post("/charge", (req, res) => {
    try {
        stripe.customers
            .create({
                name: req.body.name,
                email: req.body.email,
                source: req.body.stripeToken
            })
            .then(customer =>
                stripe.charges.create({
                    amount: req.body.amount * 100,
                    currency: "usd",
                    customer: customer.id
                })
            )
            .then(() => res.json({ success: true, message: "payment done!"}))
            .catch(err => {});
    } catch (err) {
        res.send(err);
    }
});

app.get('/api/account/login', async (req, res) => {
    if (req.query.email == undefined || req.query.password == undefined) {
        return res.json({ success: false, errors: ['invalid-request'] });
    }

    const { email, password } = req.query;
    const result = await manager.verifyLoginDetails(email, password);

    return res.json({ success: result, errors: [] });
});

app.post('/api/account/register', async (req, res) => {
    const { country, firstName, lastName, email, password, confirmPassword, address, city, state, zip, mobile } = req.body;
    console.log(req.body);

    //Server-side validation (Passwords)
    if (password != confirmPassword) {
        return res.json({
            success: false,
            errors: ['password']
        });
    }

    const searchResult = await manager.accountExists(email);
    if (searchResult) {
        return res.json({
            success: false,
            errors: ['email-exists']
        });
    }

    const creationResult = await manager.registerAccount(country, firstName, lastName, email, password, address, city, state, zip, mobile);
    if (creationResult.length == 0) {
        return res.json({
            success: true,
            message: `Account has been created!`,
            errors: []
        });
    }

    return res.json({
        success: false,
        message: `Failed to create account`,
        errors: creationResult
    });
});

/// EXPERTS ///
app.post('/api/experts', async (req, res) => {
    const { email, password, address, mobile } = req.body;
    const result = await manager.addExpert(email, password, address, mobile);

    if (result.errors.length == 0) {
        return res.json({
            errors: [],
            message: 'Expert data has been added',
            id: result.id || "NIL"
        });
    }

    return res.json({
        message: 'The request contained the following errors',
        errors: result.errors
    });
});

app.get('/api/experts/:id', async (req, res) => {
    const id = req.params.id;
    const expertData = await manager.getExpertData(id);
    return res.send(expertData);
});

app.put('/api/experts/:id', async (req, res) => {
    const id = req.params.id;
    const { email, password, address, mobile } = req.body;

    const result = await manager.updateExpertData(id, email, password, address, mobile);

    return res.json(result);
});

app.delete('/api/experts/:id', async (req, res) => {
    const id = req.params.id;

    await manager.deleteExpertData(id);

    return res.json({
        message: `Deleted expert ${id}`
    });
});

//Web Listener
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

/*  PASSPORT SETUP  */

const passport = require('passport');
var userProfile;

app.use(passport.initialize());
app.use(passport.session());

app.get('/success', (req, res) => res.send(userProfile));
app.get('/error', (req, res) => res.send("error logging in"));

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

/*  Google AUTH  */

const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const GOOGLE_CLIENT_ID = '369485809591-gauk53h0p297p7vppi08jaicocuujijt.apps.googleusercontent.com';
const GOOGLE_CLIENT_SECRET = 'Wy5IqY7ISsPojH5rbrPHNW7U';
passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "https://iservice-backend.herokuapp.com/auth/google/callback"
},
    function (accessToken, refreshToken, profile, done) {
        userProfile = profile;
        return done(null, userProfile);
    }
));

app.get('/auth/google',
    passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/error' }),
    function (req, res) {
        // Successful authentication, redirect success.
        res.redirect('/success');
    });